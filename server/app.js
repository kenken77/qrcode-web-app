require('dotenv').config();
var express = require("express");
var bodyParser = require('body-parser');
var request = require('request');

const NODE_PORT = process.env.PORT;

var app = express();

app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());

app.use(express.static(__dirname + "/../client/"));

app.get("/balance/:ether_address", function (req, res) {
    var etherAddress = req.params.ether_address;
    //https://api.ethplorer.io/getAddressInfo/0xb7C0e258F6389f593283Ac726f68693F01358b98/?apiKey=freekey
    console.log(etherAddress);
    request(`https://api.ethplorer.io/getAddressInfo/${etherAddress}/?apiKey=freekey`, function (error, response, body) {
      console.log('error:', error); 
      console.log('statusCode:', response && response.statusCode); 
      console.log('body:', response.data);
      console.log('body:', body);
      var ethInfo = JSON.parse(body);
      console.log(JSON.stringify(ethInfo));
      res.status(200).json({balance: ethInfo.ETH.balance});
    });
    

});  

app.listen(NODE_PORT, function () {
    console.log("Server running at http://localhost:" + NODE_PORT);
});
